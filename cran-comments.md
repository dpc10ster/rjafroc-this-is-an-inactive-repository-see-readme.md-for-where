## Test environments
* local OS X install, R 3.4.3
   OK
* ubuntu 12.04 (on travis-ci), R 3.4.3
   OK
* win-builder (devel and release)
   R-release: 1 Note related to 4 flagged misspelled words which are false positives
   R-devel: OK
* r-hub builder (Debian Linux, R-devel, GCC): 
   OK

## R CMD check results

0 errors | 0 warnings | 1 note

The one note was explained above.

* This is a new release.
Yes

## Reverse dependencies

This is a new release, so there are no reverse dependencies.

---

* I have run R CMD check on the NUMBER downstream dependencies.
Not applicable

* FAILURE SUMMARY
My previous version (1.0.0) installed on every platform except Solaris.
In the new submission (1.0.1) I have corrected the C++ related overloading errors. 
   However, since I do not have access to a Solaris machine, my fix is speculative 
   and may yet fail on the CRAN Solaris machine. 
My package does not install on the R-hub Solaris machine, as it depends on ggplot2, 
   which is not available for Solaris.
I request that CRAN not distribute my package on Solaris.

* All revdep maintainers were notified of the release on RELEASE DATE.
Not applicable